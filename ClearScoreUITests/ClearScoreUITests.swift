//
//  ClearScoreUITests.swift
//  ClearScoreUITests
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest

class ClearScoreUITests: XCTestCase {

	///	Application
	let application = XCUIApplication()
	
	///	Timeout
	let timeout: TimeInterval = 320.0
	
	///	Exist predicate
	let existsPredicate = NSPredicate(format: "exists == 1")
	
	///	Doesnt exist predicate
	let doesntExistPredicate = NSPredicate(format: "exists == 0")
	
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        application.launch()
    }

    override func tearDown() {
		
    }
	
	/**
		Loading Data
	*/
	public func loadingDataSuccess() {
		/// Activity intialy visible and everything else hidden
		let activityVisible = expectation(for: existsPredicate,
										  evaluatedWith: application.activityIndicators.element(matching: XCUIElement.ElementType.activityIndicator, identifier: UITestLabels.activity) ,
										  handler: nil)
		
		
		let topLabelHidden = expectation(for: doesntExistPredicate,
									  evaluatedWith: application.staticTexts.element(matching: XCUIElement.ElementType.staticText, identifier: UITestLabels.topLabel) ,
									  handler: nil)
		let scoreLabelHidden = expectation(for: doesntExistPredicate,
										 evaluatedWith: application.staticTexts.element(matching: XCUIElement.ElementType.staticText, identifier: UITestLabels.score) ,
										 handler: nil)
		let bottomLabelHidden = expectation(for: doesntExistPredicate,
										 evaluatedWith: application.staticTexts.element(matching: XCUIElement.ElementType.staticText, identifier: UITestLabels.bottomLabel),
										 handler: nil)
		
		///	Remains hidden
		let tryAgainButtonHidden = expectation(for: doesntExistPredicate,
										 evaluatedWith: application.buttons.element(matching: XCUIElement.ElementType.button, identifier: UITestLabels.tryAgain) ,
										 handler: nil)
		
		///	Switched up
		let activityHidden = expectation(for: doesntExistPredicate,
										  evaluatedWith: application.activityIndicators.element(matching: XCUIElement.ElementType.activityIndicator, identifier: UITestLabels.activity) ,
										  handler: nil)
		
		
		let topLabelVisible = expectation(for: existsPredicate,
										 evaluatedWith: application.staticTexts.element(matching: XCUIElement.ElementType.staticText, identifier: UITestLabels.topLabel) ,
										 handler: nil)
		let scoreLabelVisible = expectation(for: existsPredicate,
										   evaluatedWith: application.staticTexts.element(matching: XCUIElement.ElementType.staticText, identifier: UITestLabels.score) ,
										   handler: nil)
		let bottomLabelVisible = expectation(for: existsPredicate,
											evaluatedWith: application.staticTexts.element(matching: XCUIElement.ElementType.staticText, identifier: UITestLabels.bottomLabel),
											handler: nil)
		
		XCTWaiter().wait(for: [activityVisible, topLabelHidden, scoreLabelHidden, bottomLabelHidden, tryAgainButtonHidden,
							   activityHidden, topLabelVisible, scoreLabelVisible, bottomLabelVisible, tryAgainButtonHidden],
						 timeout: timeout,
						 enforceOrder: true)
		
	}
	
}
