//
//  APIStructure.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import SwiftyJSON

/**
	An API Progress object when the API call conforms to APIProtocol
*/
public struct APIProtocolCompletion {
	
	/// If the call was a success
	public let isSuccess: Bool
	
	/// The raw JSON object being returned
	public let json: SwiftyJSON.JSON?
	
	/// Decoded JSON object being returned
	public let object: APIProtocol?
	
	/**
		Basic Initialiser
	*/
	public init(isSuccess: Bool, json: JSON? = nil, object: APIProtocol? = nil) {
		self.isSuccess = isSuccess
		self.json = json
		self.object = object
	}
}
