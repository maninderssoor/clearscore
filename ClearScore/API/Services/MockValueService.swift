//
//  GenericService.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Create a MockValueService Service
*/
public struct MockValueService: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "Mock Values"
	
	/// The URL for this call
	static public var urlString: String = "\(HOST_URL)prod/mockcredit/values"
	
	///	The response object
	public let creditReport: APICreditReport
	
	/**
		CodingKeys
	*/
	enum CodingKeys: String, CodingKey {
		case creditReport = "creditReportInfo"
	}
}
