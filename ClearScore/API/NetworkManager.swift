//
//  NetworkManager.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

/**
	The network manager handles all API calls to the Last.fm API
*/
public class NetworkManager {
	
	/// A network sessions
	public let networkSession: URLSession
	
	///    Configuration
	private var configuration: URLSessionConfiguration = {
		let sessionConfiguration = URLSessionConfiguration.default
		sessionConfiguration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
		
		return sessionConfiguration
	}()
	
	/**
		Initalise with a configuration
	*/
	public init() {
		self.networkSession = URLSession(configuration: self.configuration, delegate: nil, delegateQueue: nil)
	}
	
	// MARK: Public Functions
	
	/**
		A generic API call to fetch from the API where objects should conform to decodable.
	*/
	public func fetch<E: APIProtocol>(with type: E.Type, completion: ((APIProtocolCompletion) -> Void)? = nil) {
		print("[NetworkManager] Fetching Data for Object \(String(describing: type))")
		
		guard let url = URL(string: type.urlString) else {
			print("Needed a valid URL")
			completion?(APIProtocolCompletion(isSuccess: false))
			return
		}
		
		let dataTask = networkSession.dataTask(with: url) { (data, _, error) in
			
			guard let data = data, error == nil else {
				DispatchQueue.main.async {
					completion?(APIProtocolCompletion(isSuccess: false))
				}
				return
			}
			
			/// Convert to object
			do {
				
				print("[NetworkManager] Successfully fetched data for \(type)")
				let object = try JSONDecoder().decode(type, from: data)
				DispatchQueue.main.async {
					let completionResponse = APIProtocolCompletion(isSuccess: true,
																   json: JSON(data: data),
																   object: object)
					completion?(completionResponse)
				}
				
			} catch {
				print("[NetworkManager] Error in catch \(String(describing: error))")
				DispatchQueue.main.async {
					let errorResponse = APIProtocolCompletion(isSuccess: false)
					completion?(errorResponse)
				}
				return
			}
			
		}
		
		dataTask.resume()
	}
	
}
