//
//  GenericResponse.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Credit Report response object
*/
public struct APICreditReport: Decodable {
	
	///	ID
	public let id: String
	
	///	The credit score
	public let score: Int
	
	///	The maximum score value
	public let maxScoreValue: Int
	
	///	The status of the report
	public let status: String
	
	/**
		Coding Keys
	*/
	enum CodingKeys: String, CodingKey {
		case id = "clientRef"
		case score
		case maxScoreValue
		case status
	}
	
}
