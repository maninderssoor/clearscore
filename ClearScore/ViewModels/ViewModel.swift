//
//  ViewModel.swift
//  ClearScore
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	A View Model for the single View Controller
*/
public class ViewModel {
	
	///	A networking manager
	internal let networkingManager = NetworkManager()
	
	///	A credit score
	internal var creditReport: APICreditReport?
	
	// MARK: Methods
	
	/**
		Fetches data from the API
	*/
	public func loadData(completion: ((Bool) -> Void)? = nil){
		networkingManager.fetch(with: MockValueService.self) { [weak self](results) in
			
			guard let service = results.object as? MockValueService, let self = self else {
				print("Error loading data from API")
				completion?(false)
				return
			}
			
			self.creditReport = service.creditReport
			completion?(true)
		}
	}
	
	/**
		Returns the score
	*/
	public func score() -> String? {
		guard let report = creditReport else {
			print("No score to return")
			return nil
		}
		
		return "\(report.score)"
	}
	
	/**
		Max score
	*/
	public func maxScore() -> String? {
		guard let report = creditReport else {
			print("No score to return max score")
			return nil
		}
		
		return "\(report.maxScoreValue)"
	}
	
	/**
		Returns the percetange score
	*/
	public func scorePercentage() -> Float? {
		guard let report = creditReport else {
			print("No report to return score percentage")
			return nil
		}
		
		return Float(report.score) / Float(report.maxScoreValue)
	}
}
