//
//  CircleLoader.swift
//  ClearScore
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	A circle loader that mimics the donught loading view
*/
public class CircleLoader: UIView {
	
	// MARK: UI
	
	///	The loading line Layer
	internal let loadingLine = CAShapeLayer()

	///	The gradient
	internal let gradientLayer = CAGradientLayer.appGradientLayer()
	
	///	The current progress
	private var progress: CGFloat = 0.0
	
	///	Animation name
	private let kAnimationName = "strokeEndAnimation"
	
	// MARK: Lifecycle
	
	/**
		Call setup
	*/
	public override func awakeFromNib() {
		super.awakeFromNib()
		
		setup()
	}
	
	/**
		Call setup
	*/
	public override init(frame: CGRect) {
		super.init(frame: frame)
		
		setup()
	}
	
	/**
		Call setup
	*/
	public required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		setup()
	}
	
	/**
		Adjust corner radius
	*/
	public override func layoutSubviews() {
		super.layoutSubviews()
		
		setupDonught()
	}
	
	// MARK: Methods
	
	/**
		Shared setup
	*/
	internal func setup() {
		///	This view setup
		backgroundColor = .clear
		layer.borderColor = UIColor.black.cgColor
		layer.borderWidth = 1.0
		layer.masksToBounds = true
		clipsToBounds = true
		setupDonught()
		
		///	Loading line setup
		loadingLine.strokeColor = UIColor.black.cgColor
		loadingLine.lineWidth = 3.0
		loadingLine.fillColor = UIColor.clear.cgColor
		loadingLine.fillMode = .removed
		loadingLine.strokeStart = 0.0
		loadingLine.strokeEnd = progress
		layer.addSublayer(loadingLine)
		
		gradientLayer.frame = bounds
		layer.addSublayer(gradientLayer)
	}
	
	/**
		Sets the radius of the main view
	*/
	internal func setupDonught() {
		layer.cornerRadius = frame.width / 2.0
		let padding: CGFloat = 8.0
		let paddedFrame = CGRect(x: padding,
								 y: padding,
								 width: frame.size.width - (2.0 * padding),
								 height: frame.size.height - (2.0 * padding))
		let path = UIBezierPath(roundedRect: paddedFrame,
								cornerRadius: paddedFrame.size.width / 2).cgPath
		loadingLine.path = path
		gradientLayer.mask = loadingLine
	}
	
	/**
		Animate to loading state
	*/
	public func setProgress(withProgress progress: Float?) {
		guard let progress = progress else {
			print("No progress passed through")
			return
		}
		
		let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
		pathAnimation.duration = 0.3
		pathAnimation.fromValue = 0.0
		pathAnimation.toValue = NSNumber(value: progress)
		loadingLine.add(pathAnimation, forKey: kAnimationName)
		self.progress = CGFloat(progress)
		
		perform(#selector(completeAnimation), with: nil, afterDelay: 0.2)
	}
	
	/**
		Complete animation
	*/
	@objc public func completeAnimation() {
		loadingLine.strokeEnd = progress
	}
}
