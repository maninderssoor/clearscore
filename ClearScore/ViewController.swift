//
//  ViewController.swift
//  ClearScore
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import UIKit

/**
	Main view controller that presents the UI
*/
public class ViewController: UIViewController {

	// MARK: View Models
	
	///	The view model for this view controller
	internal let viewModel = ViewModel()
	
	// MARK: UI
	
	///	An activity indicator
	@IBOutlet internal weak var activity: UIActivityIndicatorView?
	
	///	A Try again button
	@IBOutlet internal weak var buttonTryAgain: UIButton?
	
	///	Top label
	@IBOutlet internal weak var labelTop: UILabel?
	
	///	Credit score label. Seperated for animation. Could have been a single UIlabel with AttributedString.
	@IBOutlet internal weak var labelScore: UILabel?
	
	/// Bottom label
	@IBOutlet internal weak var labelBottom: UILabel?
	
	///	The Circle loading view
	@IBOutlet internal weak var loadingView: CircleLoader?
	
	// MARK: Lifecycle
	
	/**
		Call setup methods
	*/
	override public func viewDidLoad() {
		super.viewDidLoad()
	
		activity?.accessibilityIdentifier = UITestLabels.activity
		setupLoadingData(isLoading: true)
		loadData()
	}

	// MARK: Methods

	/**
		Loads the data
	*/
	public func loadData(completion: ((Bool) -> Void)? = nil) {
		viewModel.loadData { [weak self](isComplete) in
			guard isComplete else {
				self?.setupErrorLoading()
				completion?(false)
				return
			}
			
			self?.setupUILabels()
			self?.updateScore()
			self?.setupLoadingData(isLoading: false)
			completion?(true)
		}
	}
	
	/**
		Setup the view for a load state
	*/
	public func setupLoadingData(isLoading loading: Bool, completion: ((Bool) -> Void)? = nil) {
		UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: { [weak self] in
			
			self?.activity?.alpha = loading ? 1.0 : 0.0
			
			self?.labelTop?.alpha = loading ? 0.0 : 1.0
			self?.labelScore?.alpha = loading ? 0.0 : 1.0
			self?.labelBottom?.alpha = loading ? 0.0 : 1.0
			self?.loadingView?.alpha = loading ? 0.0 : 1.0
			
			self?.buttonTryAgain?.alpha = 0.0
		}) { (isComplete) in
			completion?(isComplete)
		}
		
		if loading {
			activity?.startAnimating()
			labelScore?.text = ""
			loadingView?.setProgress(withProgress: 0.0)
		}
	}
	
	/**
		Setup basic UI Label
	*/
	public func setupUILabels() {
		labelTop?.text = "Your credit score is"
		labelScore?.text = ""
		labelBottom?.text = "out of"
	}
	
	/**
		Show an error label
	*/
	public func setupErrorLoading(completion: ((Bool) -> Void)? = nil) {
		labelTop?.text = "Error loading"
		labelBottom?.text = ""
		
		setupLoadingData(isLoading: false)
		UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: { [weak self] in
			self?.buttonTryAgain?.alpha = 1.0
		}) { (isComplete) in
			completion?(isComplete)
		}
	}
	
	/**
		Sets the labels for the loaded score
	*/
	public func updateScore() {
		/// Need score and max score, else error
		guard let score = viewModel.score(), let maxScore = viewModel.maxScore() else {
			setupErrorLoading()
			return
		}
		labelScore?.text = score
		labelBottom?.text = "out of \(maxScore)"
		loadingView?.setProgress(withProgress: viewModel.scorePercentage())
	}
	
	// MARK: Actions
	
	/**
		Retry loading data
	*/
	@IBAction public func retry() {
		setupLoadingData(isLoading: true) { [weak self] (_) in
			self?.loadData()
		}
	}
}

