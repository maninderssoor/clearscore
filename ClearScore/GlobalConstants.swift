//
//  Constants.swift
//  LastFM
//
//  Created by Maninder Soor on 19/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/// Host name
let HOST_URL = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/"

/**
	UI Tests Accessibility Labels
*/
public class UITestLabels {
	
	static let activity = "ActivityIndicator"
	static let tryAgain = "TryAgain"
	static let topLabel = "TopLabel"
	static let score = "ScoreLabel"
	static let bottomLabel = "BottomLabel"
	static let loadingView = "LoadingView"
	
}
