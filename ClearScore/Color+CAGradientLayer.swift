//
//  Color+CAGradientLayer.swift
//  ClearScore
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

extension CAGradientLayer {
	
	/**
		Returns the default App gradient layer
	*/
	static public func appGradientLayer() -> CAGradientLayer {
		
		let gradientLayer = CAGradientLayer()
		gradientLayer.startPoint = CGPoint(x: 0.60, y: 0.33)
		gradientLayer.endPoint = CGPoint(x: 0.75, y: 1.0)
		gradientLayer.colors = [UIColor.orange.cgColor, UIColor.yellow.cgColor]
		
		return gradientLayer
	}
}
