//
//  APITests.swift
//  ClearScoreTests
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import ClearScore

/**
	API Tests, to check services are running correctly
*/
class APITests: ClearScoreTests {
	
	/// A networking manager
	let networkingManager = NetworkManager()
	
	/**
		Test connections API Call
	*/
	public func testConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testConnectionsAPICall")
		
		networkingManager.fetch(with: MockValueService.self) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertNotNil(results.object, "The object returned should not be nil")
			
			guard let service = results.object as? MockValueService else {
				XCTFail("The results.object wasn't a MockValueService")
				asychronousExpectation.fulfill()
				return
			}
			
			let report = service.creditReport
			XCTAssertEqual(report.id, "CS-SED-655426-708782", "The id isn't correct")
			XCTAssertEqual(report.score, 514, "The Score isn't correct")
			XCTAssertEqual(report.maxScoreValue, 700, "The Maximum Score isn't correct")
			XCTAssertEqual(report.status, "MATCH", "The Status isn't correct")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test connections no url string API Call
	*/
	public func testNoURLConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testNoURLConnectionsAPICall")
		
		networkingManager.fetch(with: APITestNoURL.self) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test networking manager bad API
	*/
	public func testBadAPICall() {
		let asychronousExpectation = expectation(description: "testBadAPICall")
		
		networkingManager.fetch(with: APITestBadURL.self) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertFalse(results.isSuccess, "The call shouldn't have been a success")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
}


/**
	No URL
*/
public struct APITestNoURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "NO URL"
	
	/// The URL for this call
	static public var urlString: String = ""
}


/**
	Fetch connection API
*/
public struct APITestBadURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "BAD URL"
	
	/// The URL for this call
	static public var urlString: String = "http://someurlthatwontwork.com"
}
