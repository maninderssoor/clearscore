//
//  ClearScoreTests.swift
//  ClearScoreTests
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import ClearScore

class ClearScoreTests: XCTestCase {

	///	Timeout for all asynchronous calls
	public let timeout = 120.0
	
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
