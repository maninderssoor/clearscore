//
//  ViewModelTests.swift
//  ClearScoreTests
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import ClearScore

/**
	View Model Test
*/
class ViewModelTests: ClearScoreTests {

	///	Mock Object
	private let mockReport = APICreditReport(id: "123456", score: 100, maxScoreValue: 500, status: "Match")
	
	/**
		Check score returned when mock report setup
	*/
	public func testScore() {
		let viewModel = ViewModel()
		viewModel.creditReport = mockReport
		
		XCTAssertNotNil(viewModel.score(), "The score should not have been nil")
	}
	
	/**
		Check max score returned when mock report setup
	*/
	public func testMaxScore() {
		let viewModel = ViewModel()
		viewModel.creditReport = mockReport
		
		XCTAssertNotNil(viewModel.maxScore(), "The max score should not have been nil")
	}
	
	/**
		Check percentage returned when mock report setup
	*/
	public func testPercentage() {
		let viewModel = ViewModel()
		viewModel.creditReport = mockReport
		
		XCTAssertNotNil(viewModel.scorePercentage(), "The percentagr should not have been nil")
	}
	
	/**
		Check score not returned
	*/
	public func testScoreNil() {
		let viewModel = ViewModel()
		XCTAssertNil(viewModel.score(), "The score should have been nil")
	}
	
	/**
		Check max score not returned
	*/
	public func testMaxScoreNil() {
		let viewModel = ViewModel()
		XCTAssertNil(viewModel.maxScore(), "The max score should have been nil")
	}
	
	/**
		Check percentage not returned
	*/
	public func testPercentageNil() {
		let viewModel = ViewModel()
		XCTAssertNil(viewModel.scorePercentage(), "The percentagr should have been nil")
	}
}
