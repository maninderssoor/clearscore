//
//  ViewControllerTests.swift
//  ClearScoreTests
//
//  Created by Maninder Soor on 02/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import ClearScore

/**
	View Controller tests.
*/
class ViewControllerTests: ClearScoreTests {

	///	Storyboard
	let storyboard = UIStoryboard(name: "Main", bundle: Bundle(identifier: ""))
	
	/**
		Test error loading
	*/
	public func testErrorLoading() {
		let asychronousExpectation = expectation(description: "testErrorLoading")
		guard let controller = storyboard.instantiateInitialViewController() as? ViewController else {
			XCTFail("The controller wasn't initialised correctly")
			return
		}
		let _ = controller.view
		
		controller.setupErrorLoading { [weak self] (isComplete) in
			
			XCTAssertNotNil(self, "Self should not be nil")
			if let labelTop = controller.labelTop?.text {
				XCTAssertEqual(labelTop, "Error loading", "The top text wasn't set correctly")
			} else {
				XCTFail("The top label is nil")
			}
			
			if let labelBottom = controller.labelBottom?.text {
				XCTAssertEqual(labelBottom, "", "The bottom text wasn't set correctly")
			} else {
				XCTFail("The bottom label is nil")
			}
			
			if let buttonTryAgain = controller.buttonTryAgain {
				XCTAssertEqual(buttonTryAgain.alpha, 1.0, "The try again buttton isn't visible")
			} else {
				XCTFail("The Try Again button is nil")
			}
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
}
