# ClearScore

[![Carthage Compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
![Code Coverage](https://img.shields.io/badge/coverage-90%25-yellow.svg)
[![iOS Platform](https://img.shields.io/badge/platform-ios-lightgrey.svg)](https://img.shields.io/badge/platform-ios-lightgrey.svg)

## Requirments:

We would like to see how well you write iOS apps to represent real concepts and problems.

Notes:

* There is no 100% correct answer. Make any decisions you feel are sensible.  
* You can use any frameworks you like but write about why you chose it. 
* Your code must be documented, compile and be testable by us. Please commit your code to a GitHub or Bitbucket repo and send us a link to the repository.

Write an application that will display a donut view which pulls the credit score information from the endpoint given. The image below shows an example of what is to be achieved.  The donut should display the correct value received from the endpoint.

You don’t need to worry about the donut progress bar, but for bonus points we would ideally like to see the black circular border.

The endpoint: https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod/mockcredit/values

The following requirements should be met:
* Use the latest version of Swift.
* The code should be production grade.
* It should compile and run.

Tips/Advice
* Testing is very important for us and you should take that into consideration when creating this demo. Even if you don't write a single test (e.g. because of time constraints), your app should be easy to test.
* Error scenarios should be taken into consideration as well and how easy it is to add them, even if you don't explicitly show them (e.g. using a UIAlertController).
* Although UI and UX are important, in this demo, we are more concerned on how you architect your application and your thought process. It should take into consideration features that might be added in the future.
* You can use any 3rd party library you wish. In case you do, be prepared to justify why you used it. You can use CocoaPods, Carthage for this.

Clean the file project structure (if you are creating one from scratch) and remove any unused methods (e.g. AppDelegate).

We understand that your time may be limited so we are not expecting a perfect solution.  Where you would have done something differently given more time, please explain what you would have done and why.

## Notes

An API service can be created by conforming to the APIProtocol protocol with the networking manager parsing the response using decodable.

The donught consists of a UIView with a border, and an inner CAShaperLayer (used to create animation), the shape also has a gradient on it.

The UI is relatively simlple, seperator so the score can eventually be coloured in the gradient layer that the loader is setup in.

In case of no network connection, a Try again Button attemps networking again.

Unit testing has been done throughout with code coverage of 90%.
![Unit Testing](https://drive.google.com/open?id=1NiVS24Y1BM6t4mPSn0SKLYe5htBLpXWx)


## System Requirements

- iOS 10.0+
- Xcode 10.0+
- Swift 4.0
- Command Line Tools 10.0+

## Documentation

Code has been document to a minumum of:

* Classes
* Functions
* Properties

Documentation can re-generated using jazz docs [Jazzy](https://github.com/realm/jazzy)

## Build

### Instructions

1. Checkout the application via Github or via a terminal.
2. Run `carthage update --platform iOS`
3. Open the application in Xcode and run.

## Author

Maninder Soor (http://manindersoor.com)
